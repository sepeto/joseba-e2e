const { devices } = require('@playwright/test');

module.exports = {
    use: {
        headless: false,
      },

     projects: [
    
    // Test against mobile viewports.
   
    {
      name: 'Mobile Safari',
      use: devices['iPhone 12'],
    },
    {
      name: 'Mobile Safari',
      use: devices['iPhone 8'],
    },
 
  ],
};