const { devices } = require('@playwright/test');

module.exports = {
    use: {
        headless: false,
      },

     projects: [
  
    {
        name: 'Firefox',
        use: {
          browserName: 'firefox',
          viewport: { width: 1200, height: 720 },
        },
      },

      {
        name: 'Chrome',
        use: {
          browserName: 'Chrome',
          viewport: { width: 1200, height: 720 },
        },
      },

      {
        name: 'Desktop Chromium',
        use: {
          browserName: 'chromium',
          viewport: { width: 1200, height: 720 },
          // Test against Chrome Beta channel.
          channel: 'chrome-beta',
        },
      },
    
   
  ],
};