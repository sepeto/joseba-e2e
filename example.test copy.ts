const { test, expect } = require('@playwright/test');

test('test', async ({ page }) => {

  // Go to https://www.amazon.com/
  await page.goto('https://www.amazon.com/');

  // Click [aria-label="Search"]
  await page.click('[aria-label="Search"]');

  // Fill [aria-label="Search"]
  await page.fill('[aria-label="Search"]', 'pc');

  // Click text=Go
  await Promise.all([
    page.waitForNavigation(/*{ url: 'https://www.amazon.com/s?k=pc&ref=nb_sb_noss_1' }*/),
    page.click('text=Go')
  ]);

  // Close page
  await page.close();

});