const { devices } = require('@playwright/test');

module.exports = {
    use: {
        headless: false,
      },

     projects: [
    /*{
      name: 'Desktop Safari',
      use: {
        browserName: 'webkit',
        viewport: { width: 1200, height: 750 },
      }
    },
    {
        name: 'Firefox',
        use: {
          browserName: 'firefox',
          // Test against Chrome Beta channel.
          channel: 'Firefox',
        },
      },*/
    // Test against mobile viewports.
    {
      name: 'Mobile Chrome',
      use: devices['Pixel 5'],
    },
    /*{
      name: 'Mobile Safari',
      use: devices['iPhone 12'],
    },
    {
      name: 'Desktop Firefox',
      use: {
        browserName: 'firefox',
        viewport: { width: 800, height: 600 },
      }
    },*/
  ],
};